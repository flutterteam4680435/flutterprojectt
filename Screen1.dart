import 'package:flutter/material.dart';

class screen1 extends StatelessWidget {
  const screen1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/loginb.jpg",
            fit: BoxFit.cover,
          ),
          Container(
            color: Colors.black45,
          ),
          Row(
            children: [
              Expanded(
                  flex: 11,
                  child: Column(
                    children: [
                      Container(
                        margin:EdgeInsets.only(top: 55),
                        child: Text(
                          "Wanderly",
                          style: TextStyle(
                              fontSize: 37,
                              color: Colors.white,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      Text("Your Ultimate Companion for Seamless"
                          "\nTravel Experience",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Expanded(child: Container()),
                      Column(
                        children: [
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Sign in With Phone Number",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                  ),)
                              ],
                            ),
                            margin: EdgeInsets.only(
                                left: 18,
                                right: 18,
                                bottom: 8
                            ),
                            padding: EdgeInsets.only(
                              top: 10,
                              bottom: 10,
                              left: 33,
                              right: 33,
                            ),

                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(32),
                                color: Colors.lightGreen
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.apple),
                                Text("Sign In with Apple",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18
                                  ),)
                              ],
                            ),
                            margin: EdgeInsets.only(
                                left: 15,
                                right: 15,
                                bottom: 8
                            ),
                            padding: EdgeInsets.only(
                                top: 10,
                                bottom: 10,
                                left: 33,
                                right: 33
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.white
                            ),
                          ),
                          Container(
                            child: Text(
                              "Don't have an Account ? Sign Up",
                              style: TextStyle(
                                  color: Colors.white
                              ),
                            ),
                            padding: EdgeInsets.all(7),
                            margin: EdgeInsets.only(bottom: 13),
                          ),
                          Container(
                            child: Text(
                              "By Creating an Account or Sign In,you agree to\nourTerms of Service and Privacy Policy",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            margin: EdgeInsets.only(bottom: 55),
                          )
                        ],
                      )
                    ],
                  )),

            ],
          )
        ],
      ),

    );
  }
}
